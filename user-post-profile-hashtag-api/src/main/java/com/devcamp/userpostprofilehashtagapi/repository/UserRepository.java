package com.devcamp.userpostprofilehashtagapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userpostprofilehashtagapi.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
