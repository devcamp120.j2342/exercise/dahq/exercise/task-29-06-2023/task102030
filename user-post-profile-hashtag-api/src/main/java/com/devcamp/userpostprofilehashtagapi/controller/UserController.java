package com.devcamp.userpostprofilehashtagapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userpostprofilehashtagapi.model.User;
import com.devcamp.userpostprofilehashtagapi.repository.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/user/all")
    public List<User> getAllUser() {
        return userRepository.findAll();

    }

}
