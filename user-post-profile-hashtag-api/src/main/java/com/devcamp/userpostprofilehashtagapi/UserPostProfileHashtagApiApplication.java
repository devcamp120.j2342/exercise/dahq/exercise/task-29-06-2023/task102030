package com.devcamp.userpostprofilehashtagapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserPostProfileHashtagApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserPostProfileHashtagApiApplication.class, args);
	}

}
